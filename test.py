import re
pattern=r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{4,6}"
address=[
    '192.168.2.1:1',
    '192.168.2.1:1234',
    '192.168..1:1234',
    '192.168..11234',
    '192.168..11234',
    ]
for add in address:
    if re.match(pattern,add):
        print(True)
    else:
        print(False)
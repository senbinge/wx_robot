class NodeNotFoundException(Exception):
    pass

class ContextNotEnteredException(Exception):
    pass
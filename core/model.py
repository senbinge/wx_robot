from typing import Optional
from pydantic import BaseModel

class AppInfo(BaseModel):
    package:Optional[str]
    activity:Optional[str]
    pid:Optional[int]
import time
try:
    from .wx_app import * 
except:
    import sys,os
    sys.path.append(os.path.abspath('.'))
    from wx.wx_app import *

WX_SPORT_RANK_ACTIVITY='.plugin.exdevice.ui.ExdeviceRankInfoUI'
WX_SPORT_LIKED_ACTIVITY='.plugin.exdevice.ui.ExdeviceLikeUI'
class SportTask:
    def __init__(self,app:WX=None) -> None:
        self.app=app if app else WX()
        self.device=self.app.device

    def go_sport_activity_by_search(self):
        # if not self.app.check_activity(WX_LAUNCH_ACTIVITY):
        self.app.lauch()
        print('打开搜索框...')
        self.app.open_search_box()
        print('搜索“微信运动”...')
        self.app.do_search('微信运动')
        print('进入微信运动...')
        self.device.click_node(className="android.widget.TextView",text="微信运动")
        self.device.wait_node_with_text('步数排行榜')
        if not self.device.check_node_exists(text='步数排行榜'): 
            raise Exception('没有进入微信运动界面！')
    
    def go_sport_activity_by_scan(self,retry=10):
        self.app.lauch()
        print('尝试寻找“微信运动”入口...')
        while retry>0:
            nodes=self.device(textContains='微信运动')
            for node in nodes:                
                node.click()
                if self.device.check_node_exists(text='步数排行榜'): 
                    print('成功进入“微信运动”！')
                    return
            self.device.scroll_vert_forward()
            retry-=1
        raise Exception('无法进入微信运动界面！')

    
    def go_rank_activity(self):
        print('进入“步数排行榜”...')
        self.device.click_node(text='步数排行榜')
        self.device.wait_node_with_text(text='排行榜')
        if not self.app.check_activity(WX_SPORT_RANK_ACTIVITY):
            raise Exception('没有进入步数排行榜！')


    def auto_like(self): 
        '''
        注意，无法识别是否点过赞，重新运行会全部点赞！
        '''
        if not self.app.check_activity(WX_SPORT_RANK_ACTIVITY):
            self.go_sport_activity_by_search()
            self.go_rank_activity()
        else:
            self.device.go_back()
            self.go_rank_activity()

        print('开始自动点赞...')
        liked_name_set=set()
        while True:
            name_node_list=self.device.get_node_list_by_rid('com.tencent.mm:id/c6d')
            if not liked_name_set:
                self_name=name_node_list.get_text()
                if not self_name:
                    raise Exception('没有发现本人的名字！')
                print('发现自己的名字：',self_name)
                liked_name_set.add(self_name)
            like_node_list=self.device.get_node_list_by_rid('com.tencent.mm:id/c5u')
            count=name_node_list.count
            #防止元素出现一半，无法点击，直接跳过
            for i in range(1,count):
                name_node=name_node_list[i]
                name=name_node.get_text()
                if name in liked_name_set:
                    continue
                like_node_list[i].click()
                liked_name_set.add(name)
                print(f'成功为{name}点赞！')
                time.sleep(1)

                if self.app.check_activity(WX_SPORT_LIKED_ACTIVITY):
                    self.device.go_back()

            if self.device.check_node_exists(text='邀请朋友'):
                like_node_list[-1].click()
                name=name_node_list[-1].get_text()
                liked_name_set.add(name)
                print(f'成功为{name}点赞！')
                break
            self.device.scroll_vert_forward()
        total_liked=len(liked_name_set)
        print('完成所有点赞，总数：'+str(total_liked))

if __name__ == '__main__':
    task=SportTask(WX())
    task.auto_like()
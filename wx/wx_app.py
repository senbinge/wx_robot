try:
    from core.device import Device
except (ModuleNotFoundError,ImportError):
    import sys,os
    sys.path.append(os.path.abspath('.'))
    from core.device import Device


WX_PACKAGE='com.tencent.mm'
WX_LAUNCH_ACTIVITY='.ui.LauncherUI'
WX_SEARCH_BOX_ID='com.tencent.mm:id/dt5'
WX_CHAT_TAB_BUTTON_ID='com.tencent.mm:id/dub'
WX_TITLE_BUTTON_ID='com.tencent.mm:id/dz'


class WX:
    def __init__(self,device:Device=None) -> None:
        if not device:
            device=Device()
        self.device=device

    def check_wx_lauched(self):
        app_info=self.device.get_current_app()
        return app_info.package==WX_PACKAGE and app_info.activity==WX_LAUNCH_ACTIVITY

    def lauch(self,go_launch_activity=True):
        self.device.launch_app(WX_PACKAGE,WX_LAUNCH_ACTIVITY)
        if go_launch_activity:
            self._go_launch_activity()

    def _go_launch_activity(self):
        self.device.try_up_find_node(WX_CHAT_TAB_BUTTON_ID)
        self.device.click_node_by_rid(WX_CHAT_TAB_BUTTON_ID)
        self.device.double_click_node_by_rid(WX_TITLE_BUTTON_ID)

    
    def open_search_box(self):
        self.device.click_node_by_rid(WX_SEARCH_BOX_ID)

    
    def do_search(self,text):
        self.device.input_text(text,True)
    
    def check_activity(self,activity):
        return self.device.get_current_app().activity==activity
    
    
        



if __name__ == '__main__':
    wx=WX()
    # wx.lauch()
    # wx.device.double_click_node_by_rid(WX_TITLE_BUTTON_ID)

